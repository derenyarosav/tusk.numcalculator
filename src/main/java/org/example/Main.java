package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Welcome to app Calculator! ");
        System.out.println("What's operation you chose? : ");
        Scanner scanner = new Scanner(System.in);
        String op = scanner.nextLine();

        if (op.equals("Addition")){
            Addition addition = new Addition();
            System.out.println("Enter the first number : ");
            addition.setA(scanner.nextDouble());
            System.out.println("Enter the second number : ");
            addition.setB(scanner.nextDouble());
            addition.setResult(addition.getA() + addition.getB());
            System.out.println("The result is : " + addition.getResult());

        } else if (op.equals("Subtraction")) {
            Subtraction subtraction = new Subtraction();
            System.out.println("Enter the first number : ");
            subtraction.setA(scanner.nextDouble());
            System.out.println("Enter the second number : ");
            subtraction.setB(scanner.nextDouble());
            subtraction.setResult(subtraction.getA() - subtraction.getB());
            System.out.println("The result is : " + subtraction.getResult());

        } else if (op.equals("Multiplication")) {
            Multiplication multiplication = new Multiplication();
            System.out.println("Enter the first number : ");
            multiplication.setA(scanner.nextDouble());
            System.out.println("Enter the second number : ");
            multiplication.setB(scanner.nextDouble());
            multiplication.setResult(multiplication.getA() * multiplication.getB());
            System.out.println("The result is : " + multiplication.getResult());

        } else if (op.equals("Division")){
            Division division = new Division();
            System.out.println("Enter the first number : ");
            division.setA(scanner.nextDouble());
            System.out.println("Enter the second number : ");
            division.setB(scanner.nextDouble());
            division.setResult(division.getA() / division.getB());
            System.out.println("The result is : " + division.getResult());
        }else {
            System.out.println("Unknown operation, please enter the correct operation name ");
        }
    }
}